<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Session;

class CalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPath()
    {
      return $this->$path;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('calendar');
    }

    public function upload() {
    // getting all of the post data
    $file = array('image' => Input::file('image'));
    // setting up rules
    $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
    // doing the validation, passing post data, rules and the messages
    $validator = Validator::make($file, $rules);
    if ($validator->fails()) {
      // send back to the page with the input data and errors
      return Redirect::to('upload')->withInput()->withErrors($validator);
    }
    else {
      // checking file is valid.
      if (Input::file('image')->isValid()) {
        $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
        
        if($extension != "ics") {
          Session::flash('success', 'Uploaded file is not valid!'); 
          return Redirect::to('upload');
        }
        $name = Input::file('image')->getClientOriginalName();
        $name = "\\" .$name;
        $path='error';
        try{
          Input::file('image')->move('C:\xampp\htdocs\simplifys\calendar',$name); // uploading file to given path
          $path='C:\xampp\htdocs\simplifys\calendar'.$name;
        }
        catch (Exception $e) {Session::flash('error', 'Uploaded file is not valid!');};
        
        // sending back with message
        session_start();
        $_SESSION['pathTransfer'] = $path;
        Session::flash('success', $path); 
        //return Redirect::to('afisarecalendar');
        return Redirect::to('preferences');
        
      }
      else {
        // sending back with error message.
        Session::flash('success', 'Uploaded file is not valid!');
        return Redirect::to('upload');
      }
    }
  }
}
