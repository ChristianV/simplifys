<?php
namespace App\Http\Controllers;
use App\Libraries\ICalendar;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;

class FlightsController extends Controller
{

    public function index()
    {
        return view('flights');
    }

    // controller action to POST to a url
    public function create() {
        $origin = Input::get('origin');
        $destination = Input::get('destination');
        $departing = Input::get('departing');

        $ch = curl_init();
        $data_string = '{"request":
                             {"passengers": {"adultCount": 1},
                             "slice": [{"origin": "' . $origin . '",
                                        "destination": "' . $destination . '",
                                        "date": "' . $departing . '" 
                                      }],
                                      "solutions": 3
                             }
              }';
        $data_array = json_decode($data_string);
        $data_final = json_encode($data_array);

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyDcCsVtmRMD3EiZ7vI2hprF1SGU6H9h8GY'); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_final);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_final))); 
        $result = curl_exec($ch); 

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $obj = json_decode($result);
        //dd($obj);
        return View::make('list')->with('obj', $obj);
        //return $obj;
    }

    //parametri
    // preferredCabin : COACH , PREMIUM_COACH, BUSINESS, FIRST
    // refundable : false or true
    // max price : USD2000.00
    public function searchFlights($origin,$destination,$departing,$price,$solutions) {
        $ch = curl_init(); 
        $data_string = '{
                            "request": {
                            "slice": [
                                {
                                "origin": "'.$origin.'",
                                "destination": "'.$destination.'",
                                "date": "'.$departing.'"
                                }
                            ],
                            "passengers": {
                              "adultCount": 1,
                              "infantInLapCount": 0,
                              "infantInSeatCount": 0,
                              "childCount": 0,
                              "seniorCount": 0
                            },
                            "solutions": '.$solutions.',
                            "maxPrice": "RON'.$price.'",
                            "refundable": true
                          }
                        }';
        $data_array = json_decode($data_string);
        $data_final = json_encode($data_array);

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyDcCsVtmRMD3EiZ7vI2hprF1SGU6H9h8GY'); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_final);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_final))); 
        $result = curl_exec($ch); 

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $obj = json_decode($result);
        
        //return View::make('list')->with('obj', $obj);
        return $obj;
        //dd($obj);
    }

    public function test($x) {
        if($x>0)return true;
        else return false;
    }
}
?>