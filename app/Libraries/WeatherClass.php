<?php
namespace App\Libraries;

class WeatherClass
{
    public function isWeatherOkCity($city,$dateDeparture)
    {
        $jsonCityCode=file_get_contents("..\app\Libraries\city.list.json");
        $cityCode=json_decode($jsonCityCode,true);

        $i=0;
        $ok=0;
        foreach($cityCode['code'] as $value)
        {
            if($city == $cityCode['code'][$i]['name'])
            {   
                $cityId=$i;
                $ok=1;
            }

            $i=$i+1;
        }

        
        if($ok==1)
        {
            $site="http://api.openweathermap.org/data/2.5/forecast/daily?id=";
            $code=$cityCode['code'][$cityId]['_id']."<br>";
            $userId="&units=metric&APPID=b591e729b45ff4d242704db4b61c3c2e";

            $jsonData=file_get_contents($site.$code.$userId);
            $data=json_decode($jsonData,true);
            
            for($i=0;$i<7;$i++)
            {
                //dt
                $dt=gmdate("Y-m-d", $data['list'][$i]['dt']);
                if($dt==$dateDeparture)
                {
                    //main
                    $main=$data['list'][$i]['weather'][0]['main'];
                    //speed
                    $clouds=$data['list'][$i]['clouds'];
                    if($main=='Clouds' && $clouds>40)
                    {
                        return false;
                    }
                    
                    $speed=$data['list'][$i]['speed']*(3.6);
                    //clouds
                    if($speed>130)
                    {
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }
        else
        {
            return true;
        }
    }
    
    public function isWeatherOk($cityOrigin,$cityDestination,$dateDeparture)
    {
        if(isWeatherOkCity($cityOrigin,$dateDeparture) && isWeatherOkCity($cityDestination,$dateDeparture))
            return true;
        return false;
    }
}
