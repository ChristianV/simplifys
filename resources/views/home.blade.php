@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1-1">
            <div class="panel panel-default">
                <div class="panel-heading">Flights</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/home') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">

                            <label class="col-md-2 control-label">Flying from:</label>

                            <div class="col-md-4">
                                <input type="origin" class="form-control" name="origin" value="{{ old('origin') }}" placeholder="Origin">

                                @if ($errors->has('origin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('origin') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-md-2 control-label">Departing:</label>

                            <div class="col-md-4">
                                <input type="departing" class="form-control" name="departing" value="{{ old('departing') }}" placeholder="yyyy-mm-dd">

                                @if ($errors->has('departing'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('departing') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">to:</label>

                            <div class="col-md-4">
                                <input type="destination" class="form-control" name="destination" value="{{ old('destination') }}" placeholder="Destination">

                                @if ($errors->has('destination'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-md-2 control-label">Returning:</label>

                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password" placeholder="yyyy-mm-dd">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-md-2 control-label">Adult:</label>

                            <div class="col-md-1-1">
                                <input type="password" class="form-control" name="password" placeholder="0-10">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-md-1 control-label">Child:</label>

                            <div class="col-md-1-1">
                                <input type="password" class="form-control" name="password" placeholder="0-10">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
@endsection
