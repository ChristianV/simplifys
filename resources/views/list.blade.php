<?php
namespace App\Http\Controllers;
use App\Libraries\ICalendar;
use App\Libraries\WeatherClass;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;
?>
@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<?php
$zero=0;
$k=0;
$titleEvent="Fly With Simplify!";
if(isWeatherOk('Bucharest','Barcelona','2016-06-13'))
{
    for($i=0;$i<3;$i++)
    {
        $length=count($obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment);
        if($length==0)
        {
            echo '
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1-1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">' . $titleEvent . '</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-8 control-label"><font size="2">Nu am gasit nici un zbor!</font></label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
        }
        for($k=0;$k<$length;$k++) //fara escala
        {
            //afisam
            if($k==0)
            {
            $saleTotal= $obj->trips->tripOption[''.$i.'']->saleTotal;
            $duration= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->duration;
            $origin= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->origin;
            $destination= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->destination;
            $departureTime= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->departureTime;
            $arrivalTime= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->arrivalTime;
                        
                        echo '
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-1-1">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">' . $titleEvent . '</div>
                                                <div class="panel-body">
                                                    <form class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Total Price:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $saleTotal . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Flight Duration:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $duration . ' min' . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Origin:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $origin . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Destination:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $destination . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Departure Time:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $departureTime . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Arrival Time:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $arrivalTime . '</font></label>
                                                        </div>';
                                                    
            $k++;                    
            }
            
            if($k>0 && $length>1) 
            {
                $originR= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->origin;
                $destinationR= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->destination;
                $departureTimeR= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->departureTime;
                $arrivalTimeR= $obj->trips->tripOption[''.$i.'']->slice[''.$zero.'']->segment[''.$k.'']->leg[''.$zero.'']->arrivalTime;
                        echo '
                                <hr>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Origin:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $originR . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Destination:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $destinationR . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Departure Time:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $departureTimeR . '</font></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label"><font size="2">Arrival Time:</font></label>
                                                            <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $arrivalTimeR . '</font></label>
                                                        </div>
                                                    ';
                $k++;
            }
            echo '                                  </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
        }
    }
}
else
{
                echo '
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1-1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">' . $titleEvent . '</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-8 control-label"><font size="2">Conditiile sunt nefavorabile pentru a zbura!</font></label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
}

function isWeatherOkCity($city,$dateDeparture)
    {
        $jsonCityCode=file_get_contents("..\app\Libraries\city.list.json");
        $cityCode=json_decode($jsonCityCode,true);

        $i=0;
        $ok=0;
        foreach($cityCode['code'] as $value)
        {
            if($city == $cityCode['code'][$i]['name'])
            {   
                $cityId=$i;
                $ok=1;
            }

            $i=$i+1;
        }

        
        if($ok==1)
        {
            $site="http://api.openweathermap.org/data/2.5/forecast/daily?id=";
            $code=$cityCode['code'][$cityId]['_id']."<br>";
            $userId="&units=metric&APPID=b591e729b45ff4d242704db4b61c3c2e";

            $jsonData=file_get_contents($site.$code.$userId);
            $data=json_decode($jsonData,true);
            
            for($i=0;$i<7;$i++)
            {
                //dt
                $dt=gmdate("Y-m-d", $data['list'][$i]['dt']);
                if($dt==$dateDeparture)
                {
                    //main
                    $main=$data['list'][$i]['weather'][0]['main'];
                    //speed
                    $clouds=$data['list'][$i]['clouds'];
                    if($main=='Clouds' && $clouds>40)
                    {
                        return false;
                    }
                    
                    $speed=$data['list'][$i]['speed']*(3.6);
                    //clouds
                    if($speed>130)
                    {
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }
        else
        {
            return true;
        }
    }
    
    function isWeatherOk($cityOrigin,$cityDestination,$dateDeparture)
    {
        if(isWeatherOkCity($cityOrigin,$dateDeparture) && isWeatherOkCity($cityDestination,$dateDeparture))
            return true;
        return false;
    }
?>
  <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
@endsection
