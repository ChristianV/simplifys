@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1-1">
            <div class="panel panel-default">
                <div class="panel-heading">Contact us</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <label class="col-md-1 control-label"></label>

                            <div class="col-md-5">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Name">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-5">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <div class="col-md-10 col-md-offset-1">
                              <textarea class="form-control" type="email" id="message" name="msg" placeholder="Message" rows="7"></textarea>  
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                          </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Send
                                </button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
@endsection
