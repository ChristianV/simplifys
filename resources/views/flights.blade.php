<?php
namespace App\Http\Controllers;
use App\Libraries\ICalendar;
use App\Libraries\WeatherClass;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;
?>
@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<?php

session_start();
$filepath=$_SESSION['pathTransfer'];

$ical   = new ICalendar($filepath);
$events = $ical->events();

$date = $events[0]['DTSTART'];

$requestFlight = new FlightsController;
$weatherIsOk   = new WeatherClass;

$zero=0;$unu=1;

//Variabilele care ne trebuie
//$cityOrigin,$cityDestination,$dateDeparture-pentru Weather
//$dataFlight=$requestFlight->searchFlights('OTP','LHR','2016-06-13',1500,1);

$eventsCount=0;
foreach ($events as $event) {
    $today = date("Y-m-d");
    $startDate=gmdate("Y-m-d", $ical->iCalDateToUnixTimestamp(@$event['DTSTART']));
                            
    if($startDate>$today)
    {
        $eventsCount++;
        $endDate=gmdate("Y-m-d", $ical->iCalDateToUnixTimestamp(@$event['DTEND']));
        $titleEvent=@$event['SUMMARY'];
        if($titleEvent=='')$titleEvent='Event';

        $statusEvent=@$event['STATUS'];
        $attendeesEvent=@$event['ATTENDEE'];

        //Locatioa Destination
        $locationEvent=@$event['LOCATION'];
        $locationEvent = explode(' ',trim($locationEvent));
        $locationEvent=$locationEvent[0];
        $locationEvent = ucfirst(strtolower($locationEvent));

        if($statusEvent=='CONFIRMED' && $locationEvent!='')
        {
            //Weather
            if(isWeatherOk('Bucharest','Barcelona','2016-06-13'))
            {
                /////////////////////////////////////////////////////////////////////////////////////////////
                //Convertim Bucuresti la OTP si Barcelona la XXX
                $dataFlight=$requestFlight->searchFlights('OTP','LHR','2016-06-13',1500,1);

                //pret cel mai bun
                /*$k=0;
                for($i=1;$i<3;$i++)
                {
                    if($dataFlight->trips->tripOption[''.$k.'']->saleTotal>$dataFlight->trips->tripOption[''.$i.'']->saleTotal &&
                    count($dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment)<3)
                        $k=$i;
                }*/
                //echo $k;
                $k=0;
                if(count($dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment)==1) //fara escala
                {
                    //afisam
                    $saleTotal= $dataFlight->trips->tripOption[''.$k.'']->saleTotal;
                    $duration= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->duration;
                    $origin= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->origin;
                    $destination= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->destination;
                    $departureTime= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->departureTime;
                    $arrivalTime= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->arrivalTime;
                    
                    echo '
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1-1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">' . $titleEvent . '</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Total Price:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $saleTotal . '</font></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Flight Duration:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $duration . ' min' . '</font></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Origin:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $origin . '</font></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Destination:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $destination . '</font></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Departure Time:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $departureTime . '</font></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label"><font size="2">Arrival Time:</font></label>
                                                        <label class="col-md-4 control-label"><font size="2" color="#c90000">' . $arrivalTime . '</font></label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }
                else
                {
                    /*
                    if(count($dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment)==2) // cu o escala
                    {
                        //afisam
                        $saleTotal= $dataFlight->trips->tripOption[''.$k.'']->saleTotal;
                        $duration= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->duration;
                        $origin= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->origin;
                        $destination= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->destination;
                        $departureTime= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->departureTime;
                        $arrivalTime= $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$zero.'']->arrivalTime;
                        
                        echo $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$unu.'']->origin;
                        echo $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$unu.'']->destination;
                        echo $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$unu.'']->departureTime;
                        echo $dataFlight->trips->tripOption[''.$k.'']->slice[''.$zero.'']->segment[''.$zero.'']->leg[''.$unu.'']->arrivalTime;
                    }
                    else*/
                        echo '
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1-1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">' . $titleEvent . '</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-8 control-label"><font size="2">Nu va putem recomanda nici un zbor pentru acest eveniment!</font></label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }
            }
            else
            {
                echo '
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1-1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">' . $titleEvent . '</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-8 control-label"><font size="2">Conditiile sunt nefavorabile pentru a zbura!</font></label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }
        }      
    }
}

function isWeatherOkCity($city,$dateDeparture)
    {
        $jsonCityCode=file_get_contents("..\app\Libraries\city.list.json");
        $cityCode=json_decode($jsonCityCode,true);

        $i=0;
        $ok=0;
        foreach($cityCode['code'] as $value)
        {
            if($city == $cityCode['code'][$i]['name'])
            {   
                $cityId=$i;
                $ok=1;
            }

            $i=$i+1;
        }

        
        if($ok==1)
        {
            $site="http://api.openweathermap.org/data/2.5/forecast/daily?id=";
            $code=$cityCode['code'][$cityId]['_id']."<br>";
            $userId="&units=metric&APPID=b591e729b45ff4d242704db4b61c3c2e";

            $jsonData=file_get_contents($site.$code.$userId);
            $data=json_decode($jsonData,true);
            
            for($i=0;$i<7;$i++)
            {
                //dt
                $dt=gmdate("Y-m-d", $data['list'][$i]['dt']);
                if($dt==$dateDeparture)
                {
                    //main
                    $main=$data['list'][$i]['weather'][0]['main'];
                    //speed
                    $clouds=$data['list'][$i]['clouds'];
                    if($main=='Clouds' && $clouds>40)
                    {
                        return false;
                    }
                    
                    $speed=$data['list'][$i]['speed']*(3.6);
                    //clouds
                    if($speed>130)
                    {
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }
        else
        {
            return true;
        }
    }
    
    function isWeatherOk($cityOrigin,$cityDestination,$dateDeparture)
    {
        if(isWeatherOkCity($cityOrigin,$dateDeparture) && isWeatherOkCity($cityDestination,$dateDeparture))
            return true;
        return false;
    }
?>

<hr class="featurette-divider">
<!-- /END THE FEATURETTES -->
@endsection
