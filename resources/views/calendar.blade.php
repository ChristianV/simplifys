@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1-1">
            <div class="panel panel-default" style="text-align: center;">
                <div class="panel-heading">Upload Your Calendar</div>
                <div class="panel-body" >

                                @if(Session::has('success'))
                                <div class="alert-box success">
                                    <h2>{!! Session::get('success') !!}</h2>
                                </div>
                                @endif

                                <label class="col-md-2 control-label">Upload ics file:</label>

                                {!! Form::open(array('url'=>'apply/upload','method'=>'POST', 'files'=>true)) !!}
                                <div class="col-md-4" class="control-group" style="text-align: center;">
                                    <div class="controls">
                                        {!! Form::file('image') !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                        <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                </div>
                                <br><br>

                                <div id="success"> </div>
                                <button type="submit" class="btn btn-primary" > 
                                    <i >{!! Form::open(array('class'=>'send-btn')) !!} {!! Form::close() !!} </i>Upload
                                </button>
                </div>
            </div>
        </div>
    </div>
</div>
  <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
@endsection

