@extends('layouts.app')

@section('content')
<div class="container marketing">

<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1-1">
            <div class="panel panel-default">
                <div class="panel-heading">Preferences</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/preferences') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">

                            <label class="col-md-2 control-label">Your Location:</label>

                            <div class="col-md-4">
                                <input type="origin" class="form-control" name="origin" value="{{ old('origin') }}" placeholder="City">

                                @if ($errors->has('origin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('origin') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary" name="DoneP" value="Done">
                                    <i class="fa fa-btn fa-user"></i>Done
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
@endsection
